exports.config = {
  namespace: 'puzle-quiz',
  outputTargets: [
    { 
      type: 'dist'
    }
  ]
};

exports.devServer = {
  root: 'www',
  watchGlob: '**/**'
};
