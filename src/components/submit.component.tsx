import { Component, Event, EventEmitter, Listen } from '@stencil/core';

@Component({
  tag: 'puzle-submit'
})
export class SubmitComponent {
    @Event()
    quizSubmitted: EventEmitter;

    @Listen('click')
    correct_quiz() {
        this.quizSubmitted.emit();
    }

  render() {
    return (
        <slot/>
    );
  }
}