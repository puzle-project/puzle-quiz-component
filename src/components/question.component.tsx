import { Component, Element, State, Method, Prop } from '@stencil/core';

@Component({
    tag: 'puzle-question'
  })
  export class QuestionComponent {
    
    @State()
    isCorrected = false;

    @Prop()
    label: string;

    @Element()
    el: HTMLElement;

    @Method()
      correct() {
          const answers = Array.from(this.el.querySelectorAll('puzle-answer'));
          this.isCorrected = true;
          return answers
                        .map((answer):number => answer['correct']()? 1. : 0.)
                        .reduce((previous, next) =>  previous + next) === answers.length;
      }

      render() {
        return (
            <div>
                <h3>{this.label}</h3>
               <slot/>
            </div>
        )        
      }
  }