import { Component, Prop, State, Method } from '@stencil/core';

@Component({
    tag: 'puzle-answer'
  })
  export class AnswerComponent {
    
    @State()
    isValidated = false;

    @State()
    isCorrected = false;

    @Prop()
    isCorrect: boolean = false;

    @Prop()
    label: string;
    
    @Method()
      correct() {
            this.isCorrected = true;
            return this.isCorrect == this.isValidated;
      }


      render() {
        if(this.isCorrected) {
            return this.answer_view_correction();
        } else {
            return (
                <div class="answer">
                    <input type="checkbox" id="{answer.id}" 
                        onClick={ () => {
                           this.isValidated = !this.isValidated;
                        }
                    }
                    />
                    <label >{this.label}</label>
                </div>
            )
        }
    }

    answer_view_correction() {
       if(this.isCorrect) {
           if(this.isValidated) {
            return (
                <div class="answer corrected right">
                    <input type="checkbox" id="{answer.id}" disabled checked/>
                    <label >{this.label}</label>
                </div>   
            );
           } else {
            return (
                <div class="answer corrected missing">
                    <input type="checkbox" id="{answer.id}" disabled/>
                    <label >{this.label}</label>
                </div>   
            );
           }
       } else {
        if(this.isValidated) {
            return (
                <div class="answer corrected wrong">
                    <input type="checkbox" id="{answer.id}" disabled checked/>
                    <label ><strike>{this.label}</strike></label>
                </div>  
            );
       } else {
        return (
            <div class="answer corrected neutral">
                <input type="checkbox" id="{answer.id}" disabled/>
                <label ><strike>{this.label}</strike></label>
            </div>  
        );
       }
    }

    }
  }