import { Component, Prop, State, Method, Element, Listen } from '@stencil/core';

@Component({
  tag: 'puzle-quiz',
  styleUrls: ['./quiz.css']
})
export class QuizComponent {

    @Prop()
    label: string;

    @State()
    points = 0;

    @State()
    corrected = false;

    @Method()
    correct()  {
           return this.correct_quiz();
    }
    
    @Element()
    el: HTMLElement;

    @Listen('quizSubmitted')
    correct_quiz() {
        this.corrected = true;
        const questions = Array.from(this.el.querySelectorAll('puzle-question'));
        this.points =  questions
            .map((question):number => question['correct']()? 1. : 0.)
            .reduce((previous, next) =>  previous + next) ;
        return this.points;
    }

    render() {
    return (
        <div class="quiz">
            <h2>{this.label}</h2>
            <slot/>
            {this.corrected}
            {this.corrected? 
                <div class="final_score">
                    <h3> Results</h3>
                    You got {this.points} points!
                </div>:
                <br/>
            }

      </div>
    );
  }
}