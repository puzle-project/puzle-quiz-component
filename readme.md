# Puzle Quiz Component

A Web Component to add a Quiz in your webpages.

## Demo

<script src='https://unpkg.com/puzle-quiz@0.1.1/dist/puzle-quiz.js'></script>

<puzle-quiz label="First Quiz">
  <puzle-question label="First Question">
    <puzle-answer  label="First good answer" is-correct="true"></puzle-answer>
    <puzle-answer  label="First Bad answer"></puzle-answer>
  </puzle-question>
  <puzle-submit><button>Valider</button></puzle-submit>
</puzle-quiz>

## Current RoadMap

- Define Multiple or Single Answer
- Add Pictures in Questions and Answer
- Return the quiz as JS Object
- Return the quiz result as JS Object
- Provide an easy way to weight the score of each answer
- Allow custom strategies for the correction (maybe an attribute to custom the JS function)

## Install

Add the follwing tag in your HTML file:

```html
<script type="text/javascript" src='https://unpkg.com/puzle-quiz@0.1.1/dist/puzle-quiz.js'></script>
```
## Use it

Put anywhere in the `<body>` tag the following lines:


```html
 <puzle-quiz label="First Quiz">
    <puzle-question label="First Question">
      <puzle-answer  label="First good answer" is-correct="true"></puzle-answer>
      <puzle-answer  label="First Bad answer"></puzle-answer>
   </puzle-question>
   <puzle-submit><button>Valider</button></puzle-submit>
 </puzle-quiz>
```



### `<puzle-quiz>`

#### Description

Required to create a quiz. You have to put in the Questions the Submission Button.

#### Attributes

**label** `<string>` optional: The quiz title 

#### Methods

**correct()** `return <number>`: Correct the quiz and return the final score

### `<puzle-question>`

#### Description

Create a question.

#### Attributes

**label** `<string>`: The question to display

#### Methods

**correct()** `return <number>`: Correct the question and return its score. If all the good answers were given, return 1, 0 otherwise

### `<puzle-answer>`

#### Description

Create an anwer. Now, each answer can be selected by a checkbox.

#### Attributes

- **label** `<string>`:  The answer to display
- **is-correct** `<boolean> false by default`:  Mark this answer as correct (must be selected by the user to give a point)

#### Methods

**correct()** `return <boolean>`: Return true if the answer is validated and is correct, false otherwise

### `<puzle-submit>`

#### Description

Request the quiz correction

#### Event

**click** -> `quizSubmitted()`: When the user click on the component, it emits the event `quizSubmitted`.

## Develop

To edit the project, clone it 
```bash
git clone https://gitlab.com/puzle-project/puzle-quiz-component
```

and run:

```bash
npm install
npm start
```

To view the build, start an HTTP server inside of the `/www` directory.

To watch for file changes during development, run:

```bash
npm run dev
```

To build the app for production, run:

```bash
npm run build
```

To run the unit tests once, run:

```
npm test
```

To run the unit tests and watch for file changes during development, run:

```
npm run test.watch
```
